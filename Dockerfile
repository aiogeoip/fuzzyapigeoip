# syntax=docker/dockerfile:1

FROM python:3.8-slim-buster

WORKDIR /src

RUN useradd --no-create-home -r -s /usr/sbin/nologin geoip && chown -R geoip /src

COPY --chown=geoip:geoip requirements.txt requirements.txt

RUN pip3 install --upgrade --user pip
RUN pip3 install -r requirements.txt

COPY --chown=geoip:geoip ./fuzzy.py app/
COPY --chown=geoip:geoip ./version.py app/
COPY --chown=geoip:geoip ./domains.txt app/

USER geoip

WORKDIR /src/app

CMD ["python3", "fuzzy.py"]
