import asyncio
import random
import time
import socket
import os
import pathlib

import aiohttp
import click

from loguru import logger
from dotenv import dotenv_values

from version import __version__

BASE_DIR = pathlib.Path(__file__).parent
envpath = BASE_DIR.joinpath('.env')
config = {
    **dotenv_values(envpath),  # load shared development variables
    **os.environ,  # override loaded values with environment variables
}


async def worker(name, queue, api_url):
    while True:
        # Get a "work item" out of the queue.
        ip = await queue.get()

        async with aiohttp.ClientSession() as session:
            async with session.get(f'{api_url}/v1/{ip}') as resp:
                pass    # TODO check result from api

        # Notify the queue that the "work item" has been processed.
        queue.task_done()

        logger.debug(f'{name} get geolocation from {ip}.')


async def main(url: str, number_workers: int):
    # Create a queue that we will use to store our "workload".
    queue = asyncio.Queue()
    total_number_ips = 0

    # Get the ips that will be used from a file.
    for dns in open('domains.txt', encoding='utf-8', errors='ignore').readlines():
        dns = dns.lower().replace('\n', '')
        try:
            ip = socket.gethostbyname(dns)
            queue.put_nowait(ip)
        except socket.gaierror as err:
            logger.warning(f"error get ip from dns '{dns}', {err}")
        else:
            logger.debug(f"{dns}:{ip}")
            total_number_ips += 1

    # Create three worker tasks to process the queue concurrently.
    tasks = []
    for i in range(number_workers):
        task = asyncio.create_task(worker(f'worker-{i}', queue, url))
        tasks.append(task)

    # Wait until the queue is fully processed.
    started_at = time.monotonic()
    await queue.join()
    total_slept_for = time.monotonic() - started_at

    # Cancel our worker tasks.
    for task in tasks:
        task.cancel()
    # Wait until all worker tasks are cancelled.
    await asyncio.gather(*tasks, return_exceptions=True)

    logger.info('====')
    logger.info(f'{number_workers} workers slept in parallel for {total_slept_for:.2f} '
                f'seconds with {total_number_ips} ips.')


@click.command()
@click.version_option(__version__)
@click.option('--url-api',
              help="Url api geoip.", default=config.get('API'))
@click.option('--workers',
              default=int(config.get('WORKERS')), help='Number of workers.')
def cli_fuzzy(url_api, workers):
    """
    CLI for running fuzzy test on geolocation api by ip.
    """
    asyncio.run(main(url_api, workers))


if __name__ == '__main__':
    cli_fuzzy()
