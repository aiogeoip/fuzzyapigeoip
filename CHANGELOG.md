# CHANGELOG

## 0.2

### Changes

* O argumento da URL no CLI não é mais obrigatório, caso não seja passado diretamente a ferramenta vai buscar o valor nas variáveis de ambientes (`API`).

* Arquivo renomeado de `main.py` para `fuzzy.py`.
